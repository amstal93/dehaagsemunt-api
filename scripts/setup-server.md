# Server setup

## Setup folders

    sudo mkdir -p /opt/dehaagsemunt/backend/logs
    sudo mkdir -p /opt/dehaagsemunt/backend/config
    sudo mkdir -p /opt/dehaagsemunt/backend/scripts

## Copy files

    cp ./.env /opt/dehaagsemunt/backend/config
    cp -R ./scripts/* /opt/dehaagsemunt/backend/scripts

## Setup file and folder permissions

    ## Make admin user the owner
    sudo chown arn:arn -R /opt/dehaagsemunt
    ## make gitlabrunner the owner of deploy so it can be run from the CI
    sudo chown gitlabrunner:gitlabrunner -R /opt/dehaagsemunt/backend/scripts/deploy.sh

## Add gitlabrunner user to docker group

So that the runner can execute docker commands
[Reference](https://docs.docker.com/engine/install/linux-postinstall/)

    ## Create a new group
    sudo groupadd docker
    ## Add gitlabrunner to docker group
    sudo usermod -aG docker gitlabrunner
    ## Refresh groups
    newgrp docker
    ## validate settings
    sudo su - gitlabrunner
    docker run hello-world
    exit

## SSH

- Create ed91445 keypair
- add key as gitlab variable
- add public key to known_hosts
