import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Auth0Module } from '../auth0/auth0.module';
import { SyncUsersDownProvider } from './syncUsersDown.provider';
import { BalanceModule } from '../balance/balance.module';
import { TransactionModule } from '../transaction/transaction.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User]), Auth0Module, BalanceModule, TransactionModule],
  controllers: [UserController],
  providers: [SyncUsersDownProvider, UserService],
  exports: [UserService],
})
export class UserModule {}
