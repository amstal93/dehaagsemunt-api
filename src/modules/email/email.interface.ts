import { Balance } from "../balance/balance.entity"
import { Payment } from "../payment/payment.entity"

export class EmailTemplateData {
  type: string
  to: string
  from?: string
  nickname?: string
  messageCount?: number
  transactionsCount?: number
  currentBalance?: Balance
  previousBalance?: Balance 
  payment?: Payment
}