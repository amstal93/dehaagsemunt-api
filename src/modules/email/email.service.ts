import { Injectable, Logger } from '@nestjs/common';
import { EmailTemplateData } from './email.interface';
import { templates } from './templates';
import * as SendGrid from '@sendgrid/mail';
import { MailDataRequired } from '@sendgrid/helpers/classes/mail';

@Injectable()
export class EmailService {
  private logger = new Logger('emailService');

  constructor() {
    SendGrid.setApiKey(process.env.EMAIL_SENDGRID_SECRET);
  }

  async SendEmail(data: EmailTemplateData): Promise<any> {
    let result;
    const a = templates[data.type](data);

    const msg: MailDataRequired = {
      personalizations: [
        {
          to: a.to,
          subject: a.subject,
        },
      ],
      from: {
        name: 'De Haagse Munt',
        email: 'info@dehaagsemunt.nl',
      },
      html: a.html,
      dynamicTemplateData: {
        html: a.html,
        subject: a.subject,
      },
      text: a.text,
      templateId: process.env.EMAIL_SENDGRID_TEMPLATE,
      categories: ['De Haagse Munt', process.env.NODE_ENV],
    };

    try {
      result = await SendGrid.send(msg);
    } catch (error) {
      Logger.warn(error);
      if (error.response) {
        console.error(error.response.body);
      }
    }

    this.logger.log(`Email delivery status: ${result}`);
    return result;
  }
}
