import { EmailTemplateData } from '../email.interface';
import { MailData } from '@sendgrid/helpers/classes/mail';

const MonthlyStatusEmailTemplate = (data:EmailTemplateData): MailData => {
  
  const currentBalance = data.currentBalance ? data.currentBalance.amount : 0; 
  const previousBalance = data.previousBalance ? data.previousBalance.amount : 0; 

  return {
    to: data.to, 
    from: data.from,
    subject: `[De Haagse Munt] - Maandelijks overzicht`,

    html: `<p>Hallo ${data.nickname},</p>
    <br/>
    <p>Hierbij ontvang je het maandelijks overzicht van jouw account op De Haagse Munt. </p>
    <br/>
    <h3>Details van afgelopen maand</h3>
    <p><strong>Huidig tegoed: ${currentBalance} HM</strong></p>
    <p>Vorig tegoed: ${previousBalance} HM</p>
    <p>Aantal transacties: ${data.transactionsCount}</p>
    <br/>
    <br/>
    <p>Bekijk alle details op je Haagse Munt Dashboard, handel lokaal!</p>
    <br/>
    <p>De Haagse Munt</p>`
  };
};

export default MonthlyStatusEmailTemplate;