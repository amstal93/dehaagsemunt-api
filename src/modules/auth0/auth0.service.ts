/* eslint-disable @typescript-eslint/camelcase */
import { Injectable, Logger } from '@nestjs/common';
import * as Auth0 from 'auth0';
import { UpdateAuth0UserDto } from './auth0.interface';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class Auth0Service {
  private readonly logger = new Logger(Auth0Service.name);
  private auth0Client: Auth0.ManagementClient;

  constructor(private readonly configService: ConfigService) {
    this.logger.log('init');
    const ManagementClient = Auth0.ManagementClient;
    const auth0Config = {
      domain: this.configService.get('AUTH0_DOMAIN'),
      clientId: this.configService.get('AUTH0_CLIENTID'),
      clientSecret: this.configService.get('AUTH0_CLIENTSECRET'),
    };
    this.auth0Client = new ManagementClient(auth0Config);
  }

  /**
   *
   * Fetch users from Auth0
   *
   **/
  async GetAuth0Users(
    // eslint-disable-next-line @typescript-eslint/camelcase
    per_page = 50,
    page = 0,
  ): Promise<Auth0.User<Auth0.AppMetadata, Auth0.UserMetadata>[]> {
    // eslint-disable-next-line @typescript-eslint/camelcase
    return await this.auth0Client.getUsers({ per_page, page });
  }

  /**
   *
   * Fetch a user from auth0
   *
   **/
  async GetAuth0User(sub: string): Promise<Auth0.User> {
    // logger.info(`[ auth0.service ] GetAuthOUser - Fetching user ${sub}`)
    return await this.auth0Client.getUser({ id: sub });
  }

  /**
   *
   * Update user profile at auth0;
   * @param sub
   * @param data
   *
   **/
  async UpdateAuth0User(sub: string, data: UpdateAuth0UserDto): Promise<boolean> {
    // logger.info(`[ auth0.service ] UpdateAuth0User - Update user ${sub} with data ${JS§ON.stringify(data)}`);
    const params = { id: sub };
    let result: boolean;

    try {
      await this.auth0Client.updateUser(params, data);
      this.logger.log(`UpdateAuth0User - user core data updated`);

      if (data.user_metadata && Object.keys(data.user_metadata).length > 0) {
        await this.auth0Client.updateAppMetadata(params, data.user_metadata);
        this.logger.log(`UpdateAuth0User - user app_metadata updated`);
      }

      if (data.app_metadata && Object.keys(data.app_metadata).length > 0) {
        await this.auth0Client.updateUserMetadata(params, data.app_metadata);
        this.logger.log(`UpdateAuth0User - user user_metadata updated`);
      }
      result = true;
    } catch (err) {
      this.logger.warn(`UpdateAuth0User - Error Updating user. `);
      this.logger.error(err);
      result = false;
    }

    return result;
  }

  /**
   *
   * Delete a user from Auth0
   *
   */
  async DeleteAuth0User(sub: string): Promise<void> {
    // logger.info(`Deleting user with id: ${sub}`);
    return await this.auth0Client.deleteUser({ id: sub });
  }

  // // Cast a user to an auth0 object
  // CastUserToAuth0 = (user: UpdateUserDto): UpdateAuth0UserDto => {
  //   // logger.debug(`[ user.service ] CastUserToAuth0 - Initiating user mapping`);
  //   const a: UpdateAuth0UserDto = {
  //     nickname: user.nickname,
  //     app_metadata: {
  //       last_email_notification: user.last_email_notification,
  //       account_plan: user.account_plan,
  //       account_plan_end: user.account_plan_end,
  //     },
  //     user_metadata: {
  //       firstname: user.firstname,
  //       lastname: user.lastname,
  //       bio: user.bio,
  //     },
  //     picture: user.picture,
  //   };

  //   return a;
  // }

  /**
   *
   * Helper function to generate a unique nickname.
   * @param nickname
   *
   **/
  async GenerateUniqueNickname(nickname: string): Promise<string> {
    // logger.info(`[ user.service - generateUniqueNickname ] Init ${nickname}`);

    let i = 0;
    let valid = false;
    let newNickname = nickname;
    let error = false;

    while (!valid && !error) {
      // logger.info(`[ user.service - generateUniqueNickname ] Check if nickname is already taken: ${newNickname}. `);
      try {
        const users = await this.auth0Client.getUsers({
          q: `nickname=${nickname}`,
        });
        if (users.length === 0 || !users) {
          // logger.info(`[ user.service - generateUniqueNickname ] Nickname is not yet taken. `);
          valid = true;
        } else {
          // logger.info(`[ user.service - generateUniqueNickname ] Nickname is already taken. `);
          i++;
          newNickname = nickname + i;
          // logger.info(`[ user.service - generateUniqueNickname ] New nickname is ${newNickname}. `);
        }
      } catch (err) {
        // logger.warn(`[ user.service - generateUniqueNickname ] error: `, err);
        error = true;
      }

      if (i > 50) {
        error = true;
        // logger.warn(`[ user.service - generateUniqueNickname ] Tried 50 names,aborting.`);
      }
    }

    if (error) {
      newNickname = '';
    }

    // logger.info(`[ user.service - generateUniqueNickname ] Result: ${newNickname}. `);
    return newNickname;
  }
}
