import { Logger } from '@nestjs/common';

const ValidateTotalFunds = (): boolean => {
  
  const logger = new Logger('ValidateTotalFundsCron');

  logger.log('Start cronjob.');

  // Validate:
  //   SUM of all Transaction where type = { payment + voucher }
  //   -
  //   SUM of all withdrawls
  //   ==
  //   SUM of all Latest balances
  // const transactionTotal: number = this.getTransactionTotal();
  // const withdrawlTotal: number = this.getWithdrawlTotal();
  // const balanceTotal: number = this.getBalanceTotal();
  // logger.log('Count result: ' + (transactionTotal - withdrawlTotal === balanceTotal));
  logger.log('Cronjob finished. ');

  // //
  // const getTransactionTotal = (): number => {
  //   return 100;
  // }

  // //
  // const getWithdrawlTotal = (): number => {
  //   return 20;
  // }

  // //
  // const getBalanceTotal = (): number => {
  //   return 80;
  // }

  return true;
};

export default ValidateTotalFunds;
