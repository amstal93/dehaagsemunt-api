import { Logger } from '@nestjs/common';

const RemoveDraftPayments = (): boolean => {
  const logger = new Logger('RemoveDraftPayments');
  logger.log('Initiate');

  return true;
};

export default RemoveDraftPayments;
