import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { NotificationsModule } from '../../modules/notification/notification.module';
import { EmailModule } from '../../modules/email/email.module';
import { UserModule } from '../../modules/user/user.module';
import { TransactionModule } from '../transaction/transaction.module';

@Module({
  imports: [
    NotificationsModule, 
    EmailModule, 
    UserModule, 
    TransactionModule
  ],
  providers: [
    TasksService
  ],
})

export class TasksModule {}
