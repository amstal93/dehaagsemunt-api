import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from '../user/user.entity';
import { Voucher } from '../voucher/voucher.entity';
import { Balance } from '../balance/balance.entity';

export enum TransactionType {
  PAYMENT = 'payment',
  VOUCHER = 'voucher',
  TRANSFER = 'transfer',
  WITHDRAWL = 'withdrawl',
}

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ nullable: false })
  amount!: number;

  @Column({ nullable: true, type: 'text' })
  description!: string;

  @Column({ default: false })
  processed!: boolean;

  @Column({ default: false })
  status!: boolean;

  @Column({ type: 'text', nullable: true })
  status_msg!: string;

  @CreateDateColumn({ name: 'created_at', type: 'timestamp' })
  createdAt!: Date;

  @UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
  updatedAt!: Date;

  // Associations
  @ManyToOne(
    () => User,
    user => user.transactions,
    { eager: true },
  )
  @JoinColumn({ name: 'from_id' })
  from!: User;

  @ManyToOne(
    () => User,
    user => user.transactions,
    { eager: true },
  )
  @JoinColumn({ name: 'to_id' })
  to!: User;

  @OneToMany(
    () => Balance,
    balance => balance.transaction,
  )
  balances!: Balance[];

  @ManyToOne(
    () => Voucher,
    voucher => voucher.transaction,
  )
  @JoinColumn({ name: 'voucher_id' })
  voucher!: Voucher;

  @Column({ name: 'type', type: 'enum', enum: TransactionType })
  type!: string;
}
