import { Module } from '@nestjs/common';
import { TransactionController } from '../../modules/transaction/transaction.controller';
import { TransactionService } from '../../modules/transaction/transaction.service';
import { TransactionWorkerService } from '../../modules/transaction/transaction.worker';
import { BalanceModule } from '../../modules/balance/balance.module';

@Module({
  imports: [BalanceModule],
  controllers: [TransactionController],
  providers: [TransactionWorkerService, TransactionService],
  exports: [TransactionService, TransactionWorkerService],
})
export class TransactionModule {}
