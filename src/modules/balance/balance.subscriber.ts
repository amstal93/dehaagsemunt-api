import { Connection, EntitySubscriberInterface, EventSubscriber, getRepository, InsertEvent } from 'typeorm';
import { Balance } from '../balance/balance.entity';
import { Message } from '../message/message.entity';
import { User } from '../user/user.entity';
import { CreateMessageDto } from '../message/message.interface';
import { Logger } from '@nestjs/common';

@EventSubscriber()
export class BalanceSubscriber implements EntitySubscriberInterface<Balance> {
  constructor(connection: Connection) {
    connection.subscribers.push(this);
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  listenTo() {
    return Balance;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  async afterInsert(event: InsertEvent<Balance>): Promise<void> {
    const currencyFormatter = new Intl.NumberFormat('nl-NL', {
      minimumFractionDigits: 2,
    });

    // New balance received. Send update mail to balance owner.
    // logger.debug({ message: '[ balance.subscriber ] - AfterInsert hook ', entity: event.entity });
    if (!event.entity.transaction) {
      return;
    }

    const type: string = event.entity.transaction.type;

    if (type === 'voucher') {
      Logger.log('Voucher redeemed. Create messages.');

      const messageData: CreateMessageDto = {
        type: 'system',
        user: event.entity.user,
        subject: 'Voucher verzilverd',
        body: `
          <p>Hoi</p>
          <br>
          <p>Je hebt een voucher succesvol ingewisseld voor De Haagse Munt ter waarde van: <strong>${currencyFormatter.format(
            event.entity.transaction.amount / 100,
          )} HM</strong>.</p>
          <br>
          <p>Het tegoed wordt aan je account toegevoegd. <a href="https://dashboard.dehaagsemunt.nl">https://dashboard.dehaagsemunt.nl</a></p>
          <br>
          <br>
          <p>De Haagse Munt</p>`,
      };
      const msg: Message = await event.manager.getRepository(Message).create({ ...messageData });
      await event.manager.getRepository(Message).save(msg);
    }

    // Send email for sender or recipient for this transfer
    if (type === 'transfer') {
      Logger.log('Transaction type is transfer, send a mail to the sender / recipient');

      const fromUser = await getRepository(User).findOne(event.entity.transaction.from.id);
      const toUser = await getRepository(User).findOne(event.entity.transaction.to.id);
      if (!fromUser || !toUser) {
        return null;
      }

      if (event.entity.user.id === event.entity.transaction.to.id) {
        const messageData: CreateMessageDto = {
          type: 'system',
          user: event.entity.transaction.to,
          subject: 'Tegoed ontvangen',
          body: `
            <p>Hallo ${toUser.nickname},<\\p>
            <br>
            <p>Je hebt <strong>${currencyFormatter.format(
              event.entity.transaction.amount / 100,
            )}</strong> HM ontvangen van <strong>${fromUser.nickname}</strong></p>
            <br>
            <p><strong>Transactie details:</strong></p>
            <table style="text-align:left">
              <tr><td><strong>Van: </strong></td><td>${toUser.nickname}</td></tr>
              <tr><td><strong>Aan: </strong></td><td>${fromUser.nickname}</td></tr>
              <tr><td><strong>Bedrag: </strong></td><td>${currencyFormatter.format(
                event.entity.transaction.amount / 100,
              )} HM</td></tr>
              <tr><td><strong>Beschrijving: </strong></td><td>${event.entity.transaction.description}</td></tr>
            </table>
            <br>
            <p>Bekijk het overzicht van je rekening in je <a href="https://dashboard.dehaagsemunt.nl">dashboard</a>.</p>
            <br>
            <br>
            <p>De Haagse Munt</p><br><br>`,
        };
        const msg: Message = event.manager.getRepository(Message).create({ ...messageData });
        await event.manager.getRepository(Message).save(msg);
      }

      if (event.entity.user.id === event.entity.transaction.from.id) {
        let b = `
            <p>Hallo ${fromUser.nickname},<\\p>
            <br>
            <p>Je hebt <strong>${currencyFormatter.format(
              event.entity.transaction.amount / 100,
            )}</strong> HM verstuurd naar <strong>${toUser.nickname}</strong></p>
            <br>
            <p><strong>Transactie details:</strong></p>
            <table style="text-align:left">
              <tr><td><strong>Van: </strong></td><td>${fromUser.nickname}</td></tr>
              <tr><td><strong>Aan: </strong></td><td>${toUser.nickname}</td></tr>
              <tr><td><strong>Bedrag: </strong></td><td>${currencyFormatter.format(
                event.entity.transaction.amount / 100,
              )} HM</td></tr>`;
        if (event.entity.transaction.description) {
          b += `<tr><td><strong>Beschrijving: </strong></  td><td>${event.entity.transaction.description}</td></tr>`;
        }
        b += `</table>
            <br>
            <p>Bekijk het overzicht van je rekening in je <a href="https://dashboard.dehaagsemunt.nl">dashboard</a>.</p>
            <br>
            <br>
            <p>De Haagse Munt</p><br><br>`;
        const messageData: CreateMessageDto = {
          type: 'system',
          user: event.entity.transaction.from,
          subject: 'Tegoed verstuurd',
          body: b,
        };
        const msg: Message = event.manager.getRepository(Message).create({ ...messageData });
        await event.manager.getRepository(Message).save(msg);
      }
    }
  }
}
