import { Module } from '@nestjs/common';
import { BalanceController } from './balance.controller';
import { BalanceService } from './balance.service';
import { BalanceSubscriber } from './balance.subscriber';

@Module({
  controllers: [BalanceController],
  providers: [BalanceService, BalanceSubscriber],
  exports: [BalanceService],
})
export class BalanceModule {}
