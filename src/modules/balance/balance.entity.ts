import { AfterLoad, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';
import { User } from '../user/user.entity';
import { Transaction } from '../transaction/transaction.entity';

@Entity()
export class Balance {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ nullable: true })
  amount?: number;

  @Column({ type: 'jsonb', nullable: false })
  amountmeta!: number[];

  @CreateDateColumn({
    name: 'created_at',
    type: 'timestamptz',
  })
  createdAt!: Date;

  @ManyToOne(
    () => User,
    user => user.balances,
  )
  @JoinColumn({ name: 'user_id' })
  user!: User;

  @ManyToOne(
    () => Transaction,
    transaction => transaction.balances,
  )
  @JoinColumn({ name: 'transaction_id' })
  transaction!: Transaction;

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  @AfterLoad()
  setComputed() {
    // Compute the total amount based on the amountmeta
    this.amount = Object.values(this.amountmeta).reduce((t, n) => t + n);
  }
}
