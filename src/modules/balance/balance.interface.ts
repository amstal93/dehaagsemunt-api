import { IsArray, IsNumber, ValidateNested } from 'class-validator';

class UserInBalanceDto {
  @IsNumber()
  id!: number;
}

class TransactionInBalanceDto {
  @IsNumber()
  id!: number;
}

export class CreateBalanceDto {
  @IsArray()
  amountmeta?: number[];

  @ValidateNested()
  user!: UserInBalanceDto;

  @ValidateNested()
  transaction?: TransactionInBalanceDto;
}

export interface ChangeBalanceInterface {
  old: number[];
  change: number[];
  new: number[];
}
