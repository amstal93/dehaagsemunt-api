import { ConnectionOptions } from 'typeorm';

const config: ConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'dhm',
  password: 'dhm',
  database: 'dhm',
  entities: ['./src/**/*.entity.ts'],
  cli: {
    entitiesDir: 'src',
    migrationsDir: 'src/database/migrations',
    subscribersDir: 'src/database/subscriber',
  },
};

export = config;
