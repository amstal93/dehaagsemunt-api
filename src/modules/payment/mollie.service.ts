import { Logger } from '@nestjs/common';
import { Payment } from './payment.entity';

export class MollieService {
  private mollie: any;

  // InitMollie() {
  //   // mollie = Mollie({ apiKey: config.mollie.apikey });
  // }

  /**
   *
   * Fetch a payment from Mollie
   * @param id
   */
  GetPayment(id: string): Promise<Payment> {
    return new Promise((resolve, reject) => {
      this.mollie.payments.get(id, (result: any) => {
        Logger.debug(result);
        if (result.error) {
          Logger.debug('[ mollie.service ] - There was an error! ', result.error);
          reject(result.error);
        } else {
          resolve(result);
        }
      });
    });
  }

  // Create a new payment
  CreatePayment(data: any): Promise<Payment> {
    return new Promise((resolve, reject) => {
      this.mollie.payments.create(data, (result: any) => {
        Logger.debug(result);
        if (result.error) {
          Logger.debug('[ mollie.service ] - There was an error! ', result.error);
          reject(result.error);
        } else {
          resolve(result);
        }
      });
    });
  }
}
