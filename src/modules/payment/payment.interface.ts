import { IsString, IsNumber, IsPositive, IsInt } from 'class-validator';

// class CategoryInPostDto {
//   @IsNumber()
//   public id: number;
// }

export class CreatePaymentDto {
  @IsString()
  description!: string;

  @IsNumber()
  @IsPositive()
  @IsInt()
  amount!: number;

  @IsNumber()
  @IsPositive()
  @IsInt()
  costs!: number;

  @IsNumber()
  @IsPositive()
  user_id!: number;
}

export class UpdatePaymentDto {
  @IsString()
  description?: string;

  @IsNumber()
  @IsPositive()
  @IsInt()
  amount?: number;

  @IsNumber()
  @IsPositive()
  @IsInt()
  costs?: number;

  @IsNumber()
  @IsPositive()
  user_id?: number;

  @IsString()
  status?: string;

  @IsString()
  mollie_details?: string;
}
