import {
  Controller,
  UseGuards,
  Post,
  Body,
  Req,
  Logger,
  HttpException,
  UseInterceptors,
  HttpStatus,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RedeemVoucherDto } from './voucher.interface';
import { VoucherService } from './voucher.service';
import { Voucher } from './voucher.entity';
import { SentryInterceptor } from './../../sentry.interceptor';

export interface RequestInterface extends Request {
  user: { sub: string }
  key?: string
}

@UseInterceptors(new SentryInterceptor)
@Controller('voucher')
export class VoucherController {
  
  private readonly logger = new Logger(VoucherController.name);

  constructor(private readonly voucherService: VoucherService) {
    this.logger.log('Initiated')
  }
  
  /**
   * 
   * Redeem a voucher with a provided key. 
   * @param req 
   * @param data 
   */
  @UseGuards(AuthGuard('jwt'))
  @Post('redeem')
  async redeem(@Req() req: RequestInterface, @Body() data: RedeemVoucherDto): Promise<Voucher | HttpException> {
    
    const { user } = req;
    if (!user) return new HttpException('No user povided', HttpStatus.BAD_REQUEST);

    const { key } = data; 
    if (!key) return new HttpException('No key povided', HttpStatus.BAD_REQUEST);
    
    let result: Voucher;
    try {
      result = await this.voucherService.RedeemVoucher(user.sub, key);
    } catch (err) {
      Logger.warn(err);
      throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return result;
  }
}
