# Local development 

MacOS 10.15.4
Homebrew
Docker 
Visual Studio Code 

Run development server: 

    npm run start:dev        

## Database 
Create a postgres docker instance and run it 

    docker pull postgres
    docker run --name postgres -e POSTGRES_PASSWORD=password -d postgres
    # docker (re)start | stop postgres

Using pgcli for database management: 

    brew install pgcli 
    pgcli postgres://postgres:password@localhost:54320
    -> CREATE DATABASE dhm_api
    -> CREATE USER dhm WITH ENCRYPTED PASSWORD 'dhm'


### Database commands 

| command                   | description
| --                        | --
| `\l`                      | list databases
| `CREATE DATABASE dhm_api` | Create a new database
| `use ${database_name}`    | Use database
| `\dt`                     | List tables in current database



