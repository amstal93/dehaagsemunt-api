# De Haagse Munt Deployment 

De Haagse Munt Backend API runs on a Digital Ocean droplet. 


mkdir -p /var/www/dhm/dhm_api 
cd /var/www/dhm/dhm_api 
cp docker-compose.yml ecosystem.js 

uses postgres network as defined in ~/docker-compose. With prefix.
start: docker-compose up -d

update: docker-compose pull

